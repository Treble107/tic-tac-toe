a) 
# Was bedeutet das BASEPATH in der index.php, warum steht es nicht in Anführungszeichen und wozu wird dies hier verwendet?
Zu beginn der Datei wird → 'BASEPATH' ← definiert mit 'realpath(dirname(FILE))' - realpathist eine eingebaute Funktion von PHP welche einen Pfad übergeben bekommt und einen eindeutigen und absoluten Pfad, zu dieser Stelle, zurückgibt. 
Angenommen man speichert dieses Projekt (mit der Dateistruktur aus der Aufgabenstellung) unter "c:\dev\school\tictactoe", dann würde das der BASEPATH sein. Alles dahinterliegende hat das Verzeichnis "tictactoe" als letzten gemeinsamen "Referenzpunkt". (Kann man das so nennen?)+

b) 
# Was bedeutet das DIRECTORY_SEPARATOR in der index.php und wozu wird dies hier verwendet?
Je nach Betriebssystem gibt es unterschiedliche Separatoren für Pfade, in Windows ist es der Backslash () in Linux ist es der reguläre Slash (/).
DIRECTORY_SEPARATOR ist eine globale Variable von PHP in der dieser Separator steht, da PHP weiß in welchem System es läuft.