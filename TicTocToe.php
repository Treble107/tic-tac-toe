<?php
session_start();
?>
<!DOCTYPE html>
<html Lang="DE">
<head>
    <meta charset="utf-8">
    <title>Tic-Toc-Toe</title>
    <meta name="description" content="Tic-Tac-Toe-Game. This is a small school coding project.">
    <style>
        table.tic td {
            border: 1px solid #333; /* grey cell borders */
            width: 8rem;
            height: 8rem;
            vertical-align: middle;
            text-align: center;
            font-size: 4rem;
            font-family: Arial;
        }
        table { margin-bottom: 2rem; }
        input.field {
            border: 0;
            background-color: white;
            color: white; /* make the value invisible (white) */
            height: 8rem;
            width: 8rem !important;
            font-family: Arial;
            font-size: 4rem;
            font-weight: normal;
            cursor: pointer;
        }
        input.field:hover {
            border: 0;
            color: #c81657; /* red on hover */
        }
        .colorX { color: #e77; } /* X is light red */
        .colorO { color: #77e; } /* O is light blue */
        table.tic { border-collapse: collapse; }
    </style>
</head>
<body>
    <section>
        <h1>Tic-Tac-Toe</h1>
        <article id="mainContent">
            <form method="get" action="TicTocToe.php"> 
                <?php
                $player = "X";

                $board = array();
                //the board is an array consisting of three arrays, each being one row with three cells each.
                $board = array(array("","",""),array("","",""),array("","",""));

                //Loads the Session cookie if it is present

                if( isset($_SESSION["board"]) ){
                    $board = $_SESSION["board"];
                }

                //resets the field by destroying the session cookie
                if(isset($_GET["reset"]) ){
                    session_destroy();
                    header("Location: TicTocToe.php ");
                }
                
                //make a turn and switch players
                for ($i = 0; $i < count($board); $i++){
                    for ($j = 0; $j < count($board[$i]); $j++)
                    {
                        if(isset($_GET["cell-".$i."-".$j]) ){
                            $board[$i][$j] = $_GET["cell-".$i."-".$j];
                            if ($_GET["cell-".$i."-".$j] == "X") {
                                $player = "O";
                            }else{
                                $player = "X";
                            }
                        }
                    } 
                }

                //Check if a player has won
                $winner = "";

                //Horizontal
                $counterX = 0;
                $counterO = 0;
                for ($i = 0; $i < count($board); $i++){
                    for ($j = 0; $j < count($board[0]); $j++){	
                        if ($board[$i][$j] == "X") {
                            $counterX++;
                        }else if ($board[$i][$j] == "O") {
                            $counterO++;
                        }
                    } 
                    if ($counterO == 3) {
                        $winner = "O";
                    }
                    if ($counterX == 3) {
                        $winner = "X";
                    }
                    $counterX = 0;
                    $counterO = 0;
                }

                //Vertical
                $counterX = 0;
                $counterO = 0;
                for ($j = 0; $j < count($board[0]); $j++){
                    for ($i = 0; $i < count($board); $i++){	
                        if ($board[$i][$j] == "X") {
                            $counterX++;
                        }else if ($board[$i][$j] == "O") {
                            $counterO++;
                        }
                    } 
                    if ($counterO == 3) {
                        $winner = "O";
                    }
                    if ($counterX == 3) {
                        $winner = "X";
                    }
                    $counterX = 0;
                    $counterO = 0;
                }
                if ($board[0][0] == "X" && $board[1][1] == "X" && $board[2][2] == "X") {

                }

                //sets the board session to be equal to the current board 
                $_SESSION["board"] = $board;

                //Show winner
                if ($winner != "") {
                    echo "WINNER: ".$winner;
                }

                

                //Generates the table where the game is
                if ($winner == "") {
                    echo '<table class="tic">'."\n";
                    for ($i = 0; $i < count($board); $i++){
                        echo '<tr>';
                        for ($j = 0; $j < count($board[$i]); $j++)
                            {		
                                if ($board [$i][$j] === "")
                                {
                                    echo '<td><input type="submit" class="field" name="cell-'.$i.'-'.$j.'" value="'.$player.'"></td>'; 
                                }
                                else
                                {
                                    echo "\t".'<td><span class="color'.$board [$i][$j].'">'.$board [$i][$j].'</span></td>'."\n";
                                }
                            } 
                            echo '</tr>';	
                    }
                    echo '</table>';
                }
                    ?>
                    </table>
                <input type="submit" name="reset" value="Zurücksetzen"></input>
            </form>
        </article>
    </section>
</body>
</html>
